import { API } from './api.js';
import * as UI from './interfaz.js';

UI.formularioBuscar.addEventListener('submit', e => {
	e.preventDefault();
	// Obtener datos del formulario
	const artista = document.querySelector('#artista').value,
		  cancion = document.querySelector('#cancion').value;
	// Validar datos
	if (artista === '' || cancion === '') {
		UI.divMensajes.innerHTML = '¡Error! - Todos los campos son obligatorios';
		UI.divMensajes.classList.add('error');
		setTimeout(() => {
			UI.divMensajes.innerHTML = '';
			UI.divMensajes.classList.remove('error');
		}, 3000);
	} else {
		const api = new API(artista, cancion);
		api.consultarAPI()
			.then(data => {
				if (data.respuesta.lyrics) {
					// La cancion existe
					const letra = data.respuesta.lyrics;
					UI.divResultado.textContent = letra;
				} else {
					// La cancion no existe
					UI.divMensajes.innerHTML = '¡Oops! No se encontro la canción, prueba de nuevo.';
					UI.divMensajes.classList.add('error');
					setTimeout(() => {
						UI.divMensajes.innerHTML = '';
						UI.divMensajes.classList.remove('error');
						UI.formularioBuscar.reset();
					}, 2500);
				}
			})
	}
});